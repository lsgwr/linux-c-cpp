cmake_minimum_required(VERSION 3.22)
project(fibo C)

set(CMAKE_C_STANDARD 11)

add_executable(fibo main.c)
