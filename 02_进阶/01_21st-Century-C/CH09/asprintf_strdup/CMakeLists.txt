cmake_minimum_required(VERSION 3.22)
project(asprintf_strdup C)

set(CMAKE_C_STANDARD 11)

add_executable(asprintf_strdup main.c)
