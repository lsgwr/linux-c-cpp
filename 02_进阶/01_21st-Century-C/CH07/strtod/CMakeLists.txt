cmake_minimum_required(VERSION 3.22)
project(strtod C)

set(CMAKE_C_STANDARD 11)

add_executable(strtod main.c stopif.h)
