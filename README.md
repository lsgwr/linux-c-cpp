# linux-c-cpp

> C、C++和其在Linux下的编程学习，主要是51CTO、传智播客、一些经典书籍的学习等

> Clion新建含汉字目录的项目时，CMakeLists.txt里面的`add_executable(NCRE2C main.c)`需要自己添加前面的项目名`NCRE2C`，cmake不支持汉字的项目名(即add_executable的第一个参数不能含汉字)

## Linux系统编程 学习计划
+ [Unix环境高级编程](https://book.douban.com/subject/25900403/)
  > 配套学习材料：[Linux系统编程_李慧芹_B站](https://www.bilibili.com/video/BV1yJ411S7r6):放在[02_进阶/00_尚观教育李慧芹_C和数据结构和Linux编程](02_进阶/00_尚观教育李慧芹_C和数据结构和Linux编程)中
+ [Linux/UNIX系统编程手册](https://book.douban.com/subject/25809330/)
  > 在[01_基础/linux-system-programming](01_基础/linux-system-programming)下面
+ [Linux C与C++一线开发实战](02_进阶/LinuxC与C++一线开发实践)
+ [C深度解析](02_进阶/C深度解析)
+ [Linux系统及网络编程视频](02_进阶/Linux系统及网络编程视频)

## Linux网络编程 学习计划
+ [UNIX网络编程 卷1：套接字联网API（第3版）](https://book.douban.com/subject/26434583/)
+ [UNIX网络编程 卷2：进程间通信（第2版）](https://book.douban.com/subject/26434599/)
+ [Linux网络编程（第2版）](https://book.douban.com/subject/26669330/)

## Linux内核编程 学习计划
+ [《奔跑吧Linux内核》](https://book.douban.com/subject/27108677/)
+ [《深入理解Linux内核》](https://book.douban.com/subject/1767120/)
+ [《深入Linux内核》](https://book.douban.com/subject/4843567/)

